from flask import Flask, escape, request, jsonify, make_response
import sqlite3
import json

app = Flask(__name__)

@app.route('/')
def selectCitiesByCountryCode():
    conn = sqlite3.connect('cities.db')
    c = conn.cursor()
    country_code = request.args.get('country_code')
    if country_code is None:
        return make_response('Missing parameter: country_code',400)
    try:
        c.execute('select * from cities where country_code=?', (country_code,))
        results = c.fetchall()
    except:
        return make_response('Internal Server Error',500)
    return make_response(jsonify(results), 200)
